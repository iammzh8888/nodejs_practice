CREATE DATABASE bookdb;

use bookdb;

CREATE TABLE IF NOT EXISTS `authors` (
	`id` int(11) NOT NULL,
    `email` varchar(30) NOT NULL,
    `name` varchar(30) NOT NULL,
	`writing_type` varchar(30) NOT NULL
  );

ALTER TABLE `authors` ADD PRIMARY KEY (`id`);
ALTER TABLE `authors` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `authors` (`email`, `name` , `writing_type`) VALUES
('shakespear@gmail.com', 'Shakespear', 'Romantic'),
('charles@gmail.com', 'Charles Dickens', 'Fiction'),
('tom@hotmail.com', 'Tom Cindey', 'Science');

CREATE TABLE IF NOT EXISTS `books` (
	`id` int(11) NOT NULL,
    `title` varchar(30) NOT NULL,
	`subtitle` varchar(50) NOT NULL,
    `reviews` int(10),
    `content` varchar(500) NOT NULL,
    `authorId` int(11)
  );

ALTER TABLE `books` ADD PRIMARY KEY (`id`);
ALTER TABLE `books` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `books` ADD constraint fk_book_author foreign key (authorId) references authors (id);

INSERT INTO `books` (`title`, `subtitle` , `reviews`,`content`, `authorId`) VALUES
('Hamlet', 'An awesome book for awesome people', 5, 'fdsagr ewgfdag afdsafdsafdsaf dsafsadfsdafs dafdsafdsaf', 1),
('Hamlet', 'An awesome book for awesome people', 5, 'fdsagr ewgfdag afdsafdsafdsaf dsafsadfsdafs dafdsafdsaf', 2),
('The Tempest', 'Another awesome book by your own Shakespeare', 4,'fdsagr ewgfdag afdsafdsafdsaf dsafsadfsdafs dafdsafdsaf', 1),
('The Tempest', 'Another awesome book by your own Shakespeare', 4,'fdsagr ewgfdag afdsafdsafdsaf dsafsadfsdafs dafdsafdsaf', 2);
