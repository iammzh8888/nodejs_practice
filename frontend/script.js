const HOST = "http://localhost:3000"

function getAuthors() {

    $.ajax({
        method: "GET",
        url: `${HOST}/authors`,
    }).done((authors) => {
        const authsContainerEl = document.getElementById('author-list-container');
        authsContainerEl.innerHTML = "";
        // Add data from api to FE
        if (authors.length > 0) {
            authors.forEach(author => {
                const newAuthorEl = document.createElement("div");
                newAuthorEl.className = "author-container";
                newAuthorEl.id = `author-${author.id}`;
                newAuthorEl.innerHTML =
                    `
        <h3> ${author.name} </h1>
        <div> <b>Email:</b> ${author.email} </div>
        <div> <b>Writing Type:</b> ${author.writing_type} </div>

        `
                $.ajax({
                        method: "GET",
                        url: `${HOST}/authors/${author.id}`,
                    }).done((books) => {
                        if (books.length > 0) {
                            books.forEach(book => {

                                newAuthorEl.innerHTML += `
            <div class="book-detail" id="book-${book.id}">
                <div> <b>Title:</b> ${book.title} </div>
                <div> <b>Subtitle:</b> ${book.subtitle} </div>
                <div> <b>Reviews:</b> ${book.reviews}/5 </div>

                <button onClick="getBookDetailByID(${book.id})" class="book-btn"> Book detail </button>
            </div>`

                            })
                        }
                    })
                    .fail((xhrObj, textStatus) => {
                        //An error is 400 or 500 comes from API
                        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
                            alert(xhrObj.responseJSON.message);
                        if (xhrObj && xhrObj.responseText) {
                            alert(xhrObj.responseText);
                        }
                    });
                authsContainerEl.appendChild(newAuthorEl);
            })
        }
    }).fail((xhrObj, textStatus) => {
        //An error is 400 or 500 comes from API
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
        if (xhrObj && xhrObj.responseText) {
            alert(xhrObj.responseText);
        }
    })
}

function getBookDetailByID(id) {
    const bookContainerEl = document.getElementById(`book-${id}`);
    bookContainerEl.innerHTML = "";
    // Get more book details from api
    $.ajax({
        method: "GET",
        url: `${HOST}/books/` + id,
    }).done((books) => {
        // Display book detail on html

        // Add data from api to FE
        if (books.length > 0) {
            books.forEach(book => {
                const newAuthorEl = document.createElement("div");
                newAuthorEl.className = "book-detail";
                newAuthorEl.id = `book-${book.id}`;
                newAuthorEl.innerHTML =
                    `
        <h3> ${book.title} </h1>
        <div> <b>Subtitle:</b> ${book.subtitle} </div>
        <div> <b>Reviews:</b> ${book.reviews} </div>
        <div> <b>Content:</b> ${book.content} </div>

        `
                bookContainerEl.appendChild(newAuthorEl);
            });
        }
    }).fail((xhrObj, textStatus) => {
        //An error is 400 or 500 comes from API
        if (xhrObj && xhrObj.responseJSON && xhrObj.responseJSON.message)
            alert(xhrObj.responseJSON.message);
        if (xhrObj && xhrObj.responseText) {
            alert(xhrObj.responseText);
        }
    })
}