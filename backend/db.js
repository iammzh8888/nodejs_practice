'use strict';

var mysql = require('mysql');

//local mysql db connections
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'iammzh8888',
    password: 'root',
    database: 'bookdb'
});

connection.connect(function(err) {
    if (err) throw err;
    console.log("DB connection successful");
});

module.exports = connection;