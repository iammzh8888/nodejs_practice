var express = require('express');
var router = express.Router();
const sql = require('../db');

/* GET home page. */
router.get('/', function(req, res, next) {
    sql.query('SELECT * from authors', function(error, results, fields) {
        if (error) throw error;
        res.json(results);
    })
});

/* GET one author. */
router.get('/:id', function(req, res, next) {
    sql.query('SELECT b.id,b.title,b.subtitle,b.reviews,b.authorId from books as b inner join authors as a on a.id=b.authorId where a.id=?', [req.params.id], function(error, results, fields) {
        if (error) throw error;
        res.json(results);
    })
});

module.exports = router;