var express = require('express');
var router = express.Router();
const sql = require('../db');

/* GET books by book id. */
router.get('/:id', function(req, res, next) {
    sql.query('SELECT * from books where id=?', req.params.id, function(error, results, fields) {
        if (error) throw error;
        res.json(results);
    })
});

module.exports = router;